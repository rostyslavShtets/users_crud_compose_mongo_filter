package models

import (
	"log"
	"time"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

var session *mgo.Session

func InitMongoDB(dataSourceName string) {
	var err error
	time.Sleep(1500 * time.Millisecond)
	session, err = mgo.Dial(dataSourceName)
    if err != nil {
		// for i := 1; i < 2; i++ {
		// 	fmt.Printf("We are trying to connect to db, attempt %v...\n", i)
		// 	time.Sleep(1500 * time.Millisecond)
		// 	session, err = mgo.Dial(dataSourceName)
		// 	if err == nil {
		// 		fmt.Printf("Database is connected!")
		// 		return
		// 	}
		// }
		fmt.Println("can't connect to database")
        panic(err)
    }
	// defer session.Close()
		
	session.SetMode(mgo.Monotonic, true)
	
	people := session.DB("testdb").C("people")
	
	// Index people
	indexPeople := mgo.Index{
        Key:        []string{"lastname"},
        Unique:     false,
        DropDups:   true,
        Background: true,
        Sparse:     true,
    }
    errIndexPeople := people.EnsureIndex(indexPeople)
    if errIndexPeople != nil {
        panic(errIndexPeople)
	}
	
	// _ = people.Insert(
	// 	&Person{bson.NewObjectId(), "1", "John", "Mongo", 36},
	// 	&Person{bson.NewObjectId(), "2", "Tom", "MongoDBBB", 49})

	result := Person{}
	_ = people.Find(bson.M{"lastname": "Mongo"}).One(&result)

	fmt.Println("LastName:", result)

	//books
	anotherSession := session.Copy()
	defer anotherSession.Close()
	books := anotherSession.DB("testdb").C("books")

	// Index books
	indexBooks := mgo.Index{
        Key:        []string{"author"},
        Unique:     false,
        DropDups:   true,
        Background: true,
        Sparse:     true,
    }
    errIndexBooks := books.EnsureIndex(indexBooks)
    if errIndexBooks != nil {
        panic(errIndexBooks)
	}

	// // Insert to database
	// _ = books.Insert(
	// 	&Book{bson.NewObjectId(), "54453453", "Golang WEB", "Swarz", "$34"},
	// 	&Book{bson.NewObjectId(), "54412320", "Golang server", "Balan", "$45"})

	// resultBook := Book{}
	// _ = books.Find(bson.M{"author": "Swarz"}).One(&resultBook)

	// fmt.Println("books:", resultBook)
}


// --------mysql--------
var db *sql.DB

func InitDB(dataSourceName string) {
	var err error
	db, err = sql.Open("mysql", dataSourceName)
	if err != nil {
		log.Fatal(err)
	}
	// defer db.Close()

	err = db.Ping()
	if err != nil {
		for i := 1; i < 4; i++ {
			fmt.Printf("We are trying to connect to db, attempt %v...\n", i)
			time.Sleep(1500 * time.Millisecond)
			db, _ = sql.Open("mysql", dataSourceName)
			err = db.Ping()
			if err == nil {
				fmt.Printf("Database is connected!")
				return
			}
		}
		log.Fatal(err)
	}
}