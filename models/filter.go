package models

import (
	"net/http"
	"log"
	"encoding/json"
	"github.com/go-chi/chi"
	"strconv"
	"fmt"
	"gopkg.in/mgo.v2/bson"
)

func GetPeopleFilter(w http.ResponseWriter, r *http.Request) {
	// open session
	newSession := session.Copy()
	defer newSession.Close()
	people := newSession.DB("testdb").C("people")

	// get map from query
	notice := "Query string is:" + r.Method + r.URL.String()
	log.Println(notice)
	valuesFromQuery := r.URL.Query()
	fmt.Println("values:", valuesFromQuery);
	fmt.Println("map names:", valuesFromQuery["name"]);
	fmt.Println("limit:", valuesFromQuery.Get("limit"));
	
	peopleSkipString := valuesFromQuery.Get("skip")
	peopleSkipInt, _ := strconv.Atoi(peopleSkipString)
	peopleLimitString := valuesFromQuery.Get("limit")
	peopleLimitInt, _ := strconv.Atoi(peopleLimitString)
	personAgeString := r.URL.Query().Get("age")
	personAgeInt, _ := strconv.Atoi(personAgeString)
	fmt.Println("age:", personAgeString);
	
	
	//working with name
	if valuesFromQuery.Get("name") != "" {
		namesArray := valuesFromQuery["name"]
		fmt.Println("GET params were:", namesArray);
		fmt.Println("array:", namesArray);
		

		if len(namesArray) == 1 {
			var peopleSlc []Person
			fmt.Println("qwqwqqwqwqw")
			err := people.Find(bson.M{"firstname": namesArray[0]}).All(&peopleSlc)
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}

		//if we have only more then one name
		if len(namesArray) > 1 && personAgeString == "" && valuesFromQuery.Get("limit") == ""{
			var peopleSlc []Person
			fmt.Println("namesArray")
			err := people.Find(bson.M{"firstname": bson.M{"$in": namesArray}}).All(&peopleSlc)
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}
		
		//if we have only more then one name and filter by age
		if len(namesArray) > 1 && personAgeString != "" && valuesFromQuery.Get("limit") == ""{
			var peopleSlc []Person
			fmt.Println("namesArray")
			err := people.Find(bson.M{"firstname": bson.M{"$in": namesArray}, "age": personAgeInt }).All(&peopleSlc)
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}

		//if we have only more then one name and limit it and skip
		if len(namesArray) > 1 && personAgeString == "" && valuesFromQuery.Get("limit") != "" && valuesFromQuery.Get("skip") != "" {
			fmt.Println("---if we have only more then one name and limit it---")
			var peopleSlc []Person
			fmt.Println("namesArray")
			err := people.Find(bson.M{"firstname": bson.M{"$in": namesArray}}).Skip(peopleSkipInt).Limit(peopleLimitInt).All(&peopleSlc)
			if err != nil {
				http.Error(w, http.StatusText(400), 400)
				return
			}
			json.NewEncoder(w).Encode(peopleSlc)
			return
		}

	}
	
	
	// // _ = people.Find(bson.M{}).All(&peopleSlc)
	// err := people.Find(bson.M{}).All(&peopleSlc)
	
}

func GetPerson2(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "personID")
	person := Person{}
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	// copy session
	newSession := session.Copy()
	defer newSession.Close()
	peopleColl := newSession.DB("testdb").C("people")

	err := peopleColl.FindId(bson.ObjectIdHex(id)).One(&person)
 	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	json.NewEncoder(w).Encode(person)
	
	
}
