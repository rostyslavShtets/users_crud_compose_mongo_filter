package models

import (
	"net/http"
	"log"
	"encoding/json"
	// "github.com/go-chi/chi"
	// "strconv"
	"fmt"
	"net/url"
	"gopkg.in/mgo.v2/bson"
)

func GetPeopleFilterObject(w http.ResponseWriter, r *http.Request) {
	//create istance 
	filterStruct := Filter{}
	//handle filter query
	filterFromUrl := r.URL.Query().Get("filter")
	filterJson, err:= url.QueryUnescape(filterFromUrl)
	if err != nil {
		http.Error(w, err.Error(), 400)
		return
	}
	fmt.Println("values:", filterJson);
	json.Unmarshal([]byte(filterJson), &filterStruct)
	fmt.Println("values:", filterStruct.FirstName);
	
	// open session
	newSession := session.Copy()
	defer newSession.Close()
	people := newSession.DB("testdb").C("people")

	// get map from query
	notice := "Query string is:" + r.Method + r.URL.String()
	log.Println(notice)
		

	//working with name
	if filterStruct.FirstName != "" && filterStruct.Age == 0 {
		var peopleSlc []Person
		err := people.Find(bson.M{"firstname": filterStruct.FirstName}).All(&peopleSlc)
		if err != nil {
			http.Error(w, http.StatusText(400), 400)
			return
		}
		json.NewEncoder(w).Encode(peopleSlc)
		return
	}
		
	//working with name and age
	if filterStruct.FirstName != "" && filterStruct.Age != 0 {
		fmt.Println("working with name and age")
		var peopleSlc []Person
		err := people.Find(bson.M{"firstname": filterStruct.FirstName, "age": filterStruct.Age}).All(&peopleSlc)
		if err != nil {
			http.Error(w, http.StatusText(400), 400)
			return
		}
		json.NewEncoder(w).Encode(peopleSlc)
		return
	}
		
}