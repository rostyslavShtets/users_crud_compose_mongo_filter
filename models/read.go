package models

import (
	"net/http"
	"log"
	"encoding/json"
	"github.com/go-chi/chi"
	// "database/sql"
	// "fmt"
	"gopkg.in/mgo.v2/bson"
)

func GetPeople(w http.ResponseWriter, r *http.Request) {
	newSession := session.Copy()
	defer newSession.Close()
	people := newSession.DB("testdb").C("people")
	var peopleSlc []Person
	// _ = people.Find(bson.M{}).All(&peopleSlc)
	err := people.Find(bson.M{}).All(&peopleSlc)
	if err != nil {
		log.Fatal(err)
	}
	json.NewEncoder(w).Encode(peopleSlc)
}

func GetBooks(w http.ResponseWriter, r *http.Request) {
	newSession := session.Copy()
	defer newSession.Close()
	book := newSession.DB("testdb").C("books")
	var booksSlc []Book
	// _ = people.Find(bson.M{}).All(&peopleSlc)
	err := book.Find(bson.M{}).All(&booksSlc)
	if err != nil {
		log.Fatal(err)
	}
	json.NewEncoder(w).Encode(booksSlc)
}

func GetPerson(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "personID")
	person := Person{}
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	// copy session
	newSession := session.Copy()
	defer newSession.Close()
	peopleColl := newSession.DB("testdb").C("people")

	err := peopleColl.FindId(bson.ObjectIdHex(id)).One(&person)
 	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	json.NewEncoder(w).Encode(person)
	
}

func GetBook(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "bookID")
	book := Book{}
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	// copy session
	newSession := session.Copy()
	defer newSession.Close()
	bookColl := newSession.DB("testdb").C("books")

	err := bookColl.FindId(bson.ObjectIdHex(id)).One(&book)
 	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	json.NewEncoder(w).Encode(book)
}