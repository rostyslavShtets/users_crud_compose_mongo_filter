package models

import (
	"encoding/json"
	"log"
	"net/http"
	"fmt"
	"github.com/go-chi/chi"
	"gopkg.in/mgo.v2/bson"
	// "database/sql"
	"io/ioutil"
)

func UpdatePerson(w http.ResponseWriter, r *http.Request){
	//get ID from request
	id := chi.URLParam(r, "personID")
	if id == "" {
		http.Error(w, http.StatusText(400), 400)
	}
	fmt.Println("id - ", id)

	//read JSON from request body
	body, err := ioutil.ReadAll(r.Body)
	var person Person
	if err != nil {
		log.Println("handlers SaveIDID error:", err)
		http.Error(w, "can’t read body", http.StatusBadRequest)
		return
	}
	err = json.Unmarshal(body, &person)
	if err != nil {
		log.Println("handlers SaveID error:", err)
		http.Error(w, "can’t Unmarshal json body", http.StatusBadRequest)
		return
	}

	//open session
	newSession := session.Copy()
	defer newSession.Close()
	peopleColl := newSession.DB("testdb").C("people")

	if err = peopleColl.Update(bson.M{"_id": bson.ObjectIdHex(id)}, &person); err != nil {
		fmt.Println("error with update")
		http.Error(w, http.StatusText(500), 500)
		return
	}
	fmt.Fprintf(w, "Person with ID = %v updated successfully\n", id)
}